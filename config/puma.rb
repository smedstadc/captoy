# Change to match your CPU core count
workers 1

# Min and Max threads per worker
threads 1, 6
base_dir = "/var/www/captoy"
app_dir = "#{base_dir}/current"
shared_dir = "#{base_dir}/shared"

# Default to production
rails_env = ENV['RAILS_ENV'] || "production"
environment rails_env

# Set up socket location
if ENV['RAILS_ENV'] == 'production'
  bind "unix://#{shared_dir}/sockets/puma.sock"
else
  port ENV['PORT'] || 3000
end


# Logging
stdout_redirect "#{shared_dir}/log/puma.stdout.log", "#{shared_dir}/log/puma.stderr.log", true

# Set master PID and state locations
pidfile "#{shared_dir}/pids/puma.pid"
state_path "#{shared_dir}/pids/puma.state"
activate_control_app

on_worker_boot do
  require "active_record"
  ActiveRecord::Base.connection.disconnect! rescue ActiveRecord::ConnectionNotEstablished
  ActiveRecord::Base.establish_connection(YAML.load_file("#{app_dir}/config/database.yml")[rails_env])
end
